<?php

include("diceclasses.inc.php");

$faces = $_GET["faces"];
$throws = $_GET["throws"];
$material = "Wood";
$bias = 1.2;
$results = array();
$average;

// make dice
$dice = new Dice($faces, $material);

for ($i = 1; $i<=$throws; $i++) {
   // $res = $dice->cast();
   $res = $dice->bias($bias);
    $average = $dice->average($res, $i);
    $results[] = array('id' => strval($i), 'res' => strval($res), 'avg' => $average);
	
}

$freqs = array();

for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array ('eyes' => strval($i), 'frequency' => strval($dice->getFreq($i)));
    
}
echo json_encode(array('faces'=>$faces,'material'=>$material,'results'=>$results,'frequencies'=>$freqs, 'average'=>$average));



?>