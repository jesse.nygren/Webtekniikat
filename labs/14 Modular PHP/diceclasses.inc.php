<?php

class Dice extends PhysicalDice {
    private  $faces;
    private  $freqs = array();
	private  $totalRes;
    
    // Constructor
    public function __construct($faces, $material) {
    	if ($material == null) {
    		$this->faces = $faces;
    	} else {
    		$this->faces = $faces;
    		parent::__construct($material);
    	}

    }
    
    public function cast() {
  
    		
        	$res = rand(1,$this->faces);
        	$this->freqs[$res]++;
 
        	return $res;
 
    		
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    
	public function average($result,  $throws) {
		$this->totalRes = $this->totalRes + $result;
		$finalRes = $this->totalRes / $throws;
		
		return $finalRes;
	}
	
	
	public function bias($p){
			
		if ($p == 0) {
			$res = rand(1,$this->faces);
		}
		else if (rand(0,100) < $p * 100){
			$res = $this->faces;
	
		}else {
			$res = rand(1, $this->faces - 1);
		}
		
			return $res;
	}

}

class PhysicalDice{
	
	private $material;
	
	public function __construct($material) {
 
	$this->material = $material;
    }

}

?>