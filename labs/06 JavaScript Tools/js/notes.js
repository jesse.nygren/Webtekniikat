'use strict';


var notes = new Array();
var firstItemAdded = false;
var listCounter = 0;

loadList();

function addItem() {
	var textbox = document.getElementById('item');
	var itemText = textbox.value;
	var duplicate = false;

	textbox.value = '';
	textbox.focus();

	

	if (firstItemAdded == true) {
		for (var i = 0; i < notes.length; i++) {

			if (itemText == notes[i].title) {
				duplicate = true;
				break;
			}

		}
	}

	if (duplicate == true && firstItemAdded == true) {
		var count = notes[i].quantity;
		count = count + 1;
		notes[i].quantity = count;
		saveList();
	}

	if (duplicate == false && firstItemAdded == true) {
		var newItem = { title: itemText, quantity: 1 };
		notes.push(newItem);
		saveList();
	}

	if (firstItemAdded == false) {
		var newItem = { title: itemText, quantity: 1 };
		notes.push(newItem);
		firstItemAdded = true;
		saveList();
	}

	displayList();
}

function displayList() {
	var table = document.getElementById('list');
	table.innerHTML = '';
	for (var i = 0; i < notes.length; i++) {
		var node = undefined;
		var note = notes[i];
		var node = document.createElement('tr');
		var html = '<td>' + note.title + '</td><td>' + note.quantity + '</td><td><a href="#" onClick="deleteIndex(' + i + ')">delete</td>';
		node.innerHTML = html;
		table.appendChild(node);
	}
}

function deleteIndex(i) {
	notes.splice(i, 1);
	displayList();
}

function saveList() {
	localStorage.notes = JSON.stringify(notes);
}

function loadList() {
	console.log('loadList');
	if (localStorage.notes) {
		notes = JSON.parse(localStorage.notes);
		displayList();
		firstItemAdded = true;
	}
}

var button = document.getElementById('add');
button.onclick = addItem;
