var notes = (function () {
	var list = [];

	return {
		add: function (note) {


			if (note && /\S/.test(note)) {
				var item = { timestamp: Date.now(), text: note };
				list.push(item);
				return true;
			}


			return false;
		},

		remove: function (index) {
			if (index <= list.length) {
				var item = index;
				list.splice(item, 1);
				return true;
			}

			else {
				return false;
			}

		},

		count: function () {
			return list.length;
		},

		list: function () {},

		find: function (str) {
			if (str) {
				
				for(var i = 0; i < list.length; i++){
					if(list[i].text == str){
						return i;
					}
				}


			}
			return false;
		},

		clear: function () {
			list.splice(0, list.length);
		}
	};
}());


describe('notes module', function () {
	beforeEach(function () {
		notes.clear();
		notes.add('first note');
		notes.add('second note');
		notes.add('third note');
		notes.add('fourth note');
		notes.add('fifth note');
	});

	describe('adding a note', function () {
		it('should be able to add a new note', function () {
			expect(notes.add('sixth note')).toBe(true);
			expect(notes.count()).toBe(6);
		});


		it('should ignore blank notes', function () {
			expect(notes.add('')).toBe(false);
			expect(notes.count()).toBe(5);
		});

		it('should ignore notes containing only whitespace', function () {
			expect(notes.add('   ')).toBe(false);
			expect(notes.count()).toBe(5);
		
		});

		it('should require a string parameter', function () {
			expect(notes.add()).toBe(false);
			expect(notes.count()).toBe(5);

		});
	});

	describe('deleting a note', function () {
		it('should be able to delete the first index', function () {
			expect(notes.remove(0)).toBe(true);
			expect(notes.count()).toBe(4);

		});

		it('should be able to delete the last index', function () {
			expect(notes.remove(4)).toBe(true);
			expect(notes.count()).toBe(4);

		});

		it('should return false if index is out of range', function () {
			expect(notes.remove(999)).toBe(false);
			expect(notes.count()).toBe(5);
		});

		it('should return false if the parameter is missing', function () {
			expect(notes.remove()).toBe(false);
			expect(notes.count()).toBe(5);
		});
	});

	describe('Finding index of object', function () {
		it('find index of an object', function () {
			expect(notes.find('third note')).toBe(2);


		});
	});
});
